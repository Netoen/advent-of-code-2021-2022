from defaults import *
from functools import wraps


EXAMPLE_INPUT = '''forward 5
down 5
forward 8
up 3
down 8
forward 2'''
EXAMPLE_OUTPUT = 900


def ensure_correct_input(f):
  @wraps(f)
  def fix_input(p_input: list = EXAMPLE_INPUT, **kwargs):
    if not isinstance(p_input, list):
      logging.debug('p_input was not a list, converting . . .')
      p_input = p_input.splitlines()
    return f(p_input, **kwargs)
  return fix_input


@ensure_correct_input
def main(p_input: list, **kwargs):
  logging.debug(p_input)
  sub = Submarine()
  for step in p_input:
    step = step.split()
    step[1] = int(step[1])
    match step[0].lower():
      case 'forward':
        sub.pos += step[1]
        sub.depth += sub.aim * step[1]
      case 'down':
        sub.aim += step[1]
      case 'up':
        sub.aim -= step[1]
  result = sub.depth * sub.pos
  return result, EXAMPLE_OUTPUT


class Submarine:
  def __init__(self,
               depth: int = 0,
               pos: int = 0,
               aim: int = 0
               ):
    self.__depth = depth
    self.__pos = pos
    self.__aim = aim

  @property
  def depth(self):
    return self.__depth

  @depth.setter
  def depth(self, new_depth):
    self.__depth = new_depth

  @property
  def pos(self):
    return self.__pos

  @pos.setter
  def pos(self, new_pos):
    self.__pos = new_pos

  @property
  def aim(self):
    return self.__aim

  @aim.setter
  def aim(self, new_aim):
    self.__aim = new_aim


if __name__ == '__main__':
    p = get_puzzle(2)
    p.answer_b, _ = main(p.input_data.splitlines())
