from defaults import *


something = '''199
200
208
210
200
207
240
269
260
263'''


@dataclass
class a_result:
    increments: int = 0
    decrements: int = 0
    input_list: list[int] = field(default_factory=list)

    def check_diff(self, numprev, numcur):
        my_bool = numcur > numprev
        if my_bool:
            self.increments += 1
        else:
            self.decrements += 1
        logging.debug(f'{numcur} {">" if my_bool else "<" } {numprev}, inc: {self.increments}')


def main(p_input: list, **kwargs):
    r = a_result(input_list=[int(item) for item in p_input])
    prev_summ = sum(r.input_list[:3])
    # This is gorgeous!
    for item in zip(r.input_list[2:], r.input_list[1:-1], r.input_list[:-1]):
        r.check_diff(prev_summ, prev_summ := sum(item))
    logging.info(r.increments)
    return r.increments


if __name__ == '__main__':
    p = get_puzzle(1)
    p.answer_b = main(p.input_data.splitlines())

