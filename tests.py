import unittest
from d2 import p1
try:
    from d2 import p2
except ImportError:
    pass


CONST_ERROR = "We failed at the index {i}, where {first} wasn't equal to {second}"
CONST_CHECKS = 1

tst = '''forward 5
forward 2'''

class test_my_case(unittest.TestCase):

    def test_example_1(self):
        for i in range(CONST_CHECKS):
            actual, expected = p1.main(idx=i)
            self.assertEqual(
                int(expected),
                int(actual),
                msg=CONST_ERROR.format(
                    i=i,
                    first=actual,
                    second=expected
                )
            )

    def test_example_2(self):
        for i in range(CONST_CHECKS):
            actual, expected = p2.main(idx=i)
            self.assertEqual(
                int(expected),
                int(actual),
                msg=CONST_ERROR.format(
                    i=i,
                    first=actual,
                    second=expected
                )
            )


if __name__ == "__main__":
    unittest.main()
