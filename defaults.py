import logging
import aocd
from os.path import basename
from pprint import pformat
from dataclasses import dataclass, field


aoc_year = 2021


def get_puzzle(aoc_day: int):
  try:
    return aocd.models.Puzzle(year=aoc_year, day=aoc_day)
  except NameError:
    raise NameError('Variable aoc_day was not defined when calling ' + basename(__file__))


# Use the first one if you will use log files
# logging.basicConfig(level=logging.DEBUG, format='{%(asctime)s [%(levelname)s]: %(message)s')
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s_%(msecs)03d [%(levelname)s] %(message)s', datefmt='%H:%M:%S')
